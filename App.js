
import { placeholder } from '@babel/types';
import React, { useState, useEffect }from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
  FlatList,
  Button,
  Image,
  PermissionsAndroid,
  } from 'react-native';
import HomeScreen from './components/HomeScreen';
import Profile from './components/Profile';
import DoubleClick from 'react-native-double-tap';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Login from './components/Login/Login';
import SignUp from './components/SignUp/SignUp';

const App = () => {
  const Tab= createBottomTabNavigator()
const Stack = createNativeStackNavigator();
  const[value,setValue] = useState('');
  useEffect(()=>{
    console.log(Dimensions.get('screen').width)
  },[])

  const HomeStackScreen = () => {
    return (<Stack.Navigator>

<Stack.Screen name = "LoginScreen" component= {Login} options={{headerShown: false,}}/>
<Stack.Screen name = "SignUpScreen" component= {SignUp} options={{headerShown: false,}}/>

        <Stack.Screen name = "Home" component={HomeScreen} options={{title:"My Home",headerStyle:{
          backgroundColor: 'orange',
        },
        headerTitleStyle:{ fontWeight: 'bold'},
        }}/>
        <Stack.Screen name = "Profile" component= {Profile} options={{title:"My Profile",headerStyle:{
          backgroundColor: 'orange',
        },
        headerTitleStyle:{ fontWeight: 'bold'},
        }}/>
      </Stack.Navigator>
    );
  }
const TabHomeScreen = () => {
  return(
  <Tab.Navigator screenOptions={({route}) => ({
    tabBarIcon:({focused, color,size}) => {
      let iconName;
      if(route.name == 'Home'){
        iconName = focused ? 'ios-information-circle'
        : 'ios-information-circle-outline';
      }else if (route.name == 'Profile'){
        iconName = focused ? 'ios-list-box' : 'ios-list';
      }
      
      return <Ionicons name={iconName} size={size} color={color} />;
    },
    
    tabBarActiveTintColor: 'tomato',
    tabBarInactiveTintColor: 'gray',
  })}>
    <Tab.Screen name = "Home" component={HomeStackScreen} options={{headerShown: false,}}/>
    <Tab.Screen name = "Profile" component= {Profile} initialParams={{data:1}} />
  </Tab.Navigator>);
}

  return (
    <NavigationContainer styles={{backgroundColor:'#747373'}}>
      <Login/>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({

});

export default App;
