import React, { useState } from 'react'
import { View, Text,TextInput,StyleSheet,TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native';

export default function Login({navigation}) {
    const[username,setUserName] = useState('');
    const[password,setPassword] = useState('');
    
    const handeSubmit=() => {
        if(username=='' && password==''){
            alert('Enter the Username and the password')
        }
        else {
            navigation.navigate('HomeScreen')
        }
    }

    return (
        <View style={styles.loginCointainer}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>Login</Text>
            </View>
            <View>
                <TextInput style={styles.userNameInput} value={username} placeholder='User Name' onChangeText={(e) =>setUserName(e)}></TextInput>
            </View>
            <View>
                <TextInput style={styles.passwordContainer} value={password} placeholder='Password' secureTextEntry={true} onChangeText={(e) =>setPassword(e)}></TextInput>
            </View>
            <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.button} onPress={()=>handeSubmit()}>
                <Text style={{color:'white',fontSize:15}}>Login</Text>
            </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity onPress={()=> navigation.navigate('SignUp')}>
                <Text>Not a member ? Register Here</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    loginCointainer:{
        alignItems:'center',
        alignContent:'center',
        elevation:15,
        marginTop:100,
        shadowColor:'#C7BBB8',
    },
    buttonContainer:{
        margin:10,
        alignContent:'center',
        alignItems:'center',
    },
    button:{
        alignItems:'center',
         width:120,
         backgroundColor:'#B96551',
         borderRadius:10,
         padding:10
    },
    titleContainer:{
        marginTop:20,
        alignContent:'center',
        alignItems:'center',
        marginBottom:10,
    },
    title:{
        color:'black',
        padding:5,
        fontSize:20,
    },
    passwordContainer:{
        paddingStart:3,
        paddingEnd:3,
        justifyContent:'center',
        margin:5,
        textAlign:'center',
        textDecorationColor:'black',
        elevation:5,
        shadowColor:'#C7BBB8',
        backgroundColor:'#FF8D75',
        borderRadius:15,
        width:200,
        borderColor:'orange',
    },
    userNameInput:{
        paddingStart:3,
        paddingEnd:3,
        justifyContent:'center',
        margin:5,
        textAlign:'center',
        textDecorationColor:'black',
        elevation:5,
        shadowColor:'#C7BBB8',
        backgroundColor:'#FF8D75',
        borderRadius:15,
        width:200,
        borderColor:'orange',
    }
});
