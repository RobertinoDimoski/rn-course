import React, {useState} from 'react'
import { StyleSheet, Text, View,TouchableOpacity,Button,PermissionsAndroid } from 'react-native'

export default function HomeScreen({navigation}) {
    
    const [count,setCount] = useState(0)
    const [name,setName] = useState('Robertino')
    return (
        <View>
            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Text>GO To profile</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{alignItems:'center', width:100,backgroundColor:'blue',borderRadius:10,padding:10}} onPress={()=>setCount(count+1)}>
                <Text style={{color:'white'}}>+</Text>
            </TouchableOpacity>

            <Text>Count is: {count}</Text>
            <Text onPress={()=>setName('Sara')}>{name}</Text>
        </View>
    )
}

const styles = StyleSheet.create({})
